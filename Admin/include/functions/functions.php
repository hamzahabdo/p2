<?php

/*
  ** Get All function v2.0
  ** function to get All from DB 
  */

  function getAllFrom($field ,$table ,$where = NULL , $and = NULL ,$orderField ,$ordering = 'ASC'){
    global $con;
     
       $getAll = $con -> prepare("SELECT $field FROM $table $where $and ORDER BY $orderField $ordering");
       $getAll->execute();
       $all = $getAll ->fetchAll();
       return $all;
   }


/*
  ** function v1.0
  ** Title function that echo the page title in case the page has
  variable $pageTitle And echo default title for athers pages
  */

  function getTitle(){
      global $pageTitle;
      if(isset($pageTitle)){
          echo $pageTitle;
      } else{
          echo 'default';
      }
  }

  /*
  **Home redirect function v2.0
  **[ this function Accept parameter ]
  ** $themsg = echo the massage[Error | success | Warning]
  ** $url = the link you to redirect to
  ** $seconds = seconds before redirecting
  */
  function redirectHome($errormsg,$url = null, $seconds = 3){
    if($url === null){
        $url = 'index.php';
    } else{
        $url = isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] !== '' ? $_SERVER['HTTP_REFERER']  : 'index.php' ;     
        $link = 'previous Page';
        }
        if($url === 'index.php'){
            $link = 'HomePage';
        }
    echo $errormsg;
    echo "<div class='alert alert-info'>You will be redirected to $link after $seconds seconds</div>";
    header("refresh:$seconds;url=$url");
    exit();
}
  /*
  **Home redirect function v1.0
  **[ this function Accept parameter ]
  ** $errormsg = echo the error massage
  ** $seconds = seconds before redirecting
  
  function redirectHome($errormsg, $seconds = 3){
      echo "<div class='alert alert-danger'>$errormsg</div>";
      echo "<div class='alert alert-info'>You will be redirected to HomePage after $seconds seconds</div>";
      header("refresh:$seconds;url=index.php");
      exit();
  }
  */

  //==================================
  //fuction for check the items
  /*
  function checkItem($colname,$table,$itemname){
    include 'connect.php';
    $stmt = $con->prepare("SELECT $colname FROM $table WHERE $colname = ? LIMIT 1");
    $stmt->execute(array($itemname));

    $rows = $stmt->fetchAll();
    foreach($rows as $row){
        if($row['$colname'] = $itemname ){
            return 1;
        }
    }
    return 0;
  }*/
  function checkItem($select,$from,$value){
    //include 'connect.php';
    global $con;
    $stmtt = $con->prepare("SELECT $select FROM $from WHERE $select = ?");
    $stmtt->execute(array($value));
    $count = $stmtt-> rowCount();
    return $count;
  }

  /*
  **Count number of items rows function v1.0
  ** $item = the item to count
  ** $table = the table to choose from 
  */
  function countItems($item,$table){
    global $con;
    $stmt2 = $con ->prepare("SELECT COUNT($item) FROM $table");
    $stmt2 ->execute();
    return $stmt2 ->fetchColumn();
  }

  /*
  ** Get latest records function v1.0
  ** function to get latest Items from DB  [users | items | comments]
  */
  function getLatest($select,$table,$order,$limit){
      global $con;
      $getstmt = $con -> prepare("SELECT $select FROM $table ORDER BY $order DESC LIMIT $limit");
      $getstmt->execute();
      $rows = $getstmt ->fetchAll();
      return $rows;
  }
?>