<!DOCTYPE html>
<html>
<head>
   <meta charset="UTF-8" >
   <meta http-equiv="X-UA-Compatible" content="IE-edge">
   <meta name="viewport" content="width=device-width,initial-scale=1, shrink-to-fit=no"><!-- for responsive-->
   <title><?php getTitle();?></title>
   <link rel="stylesheet" href="<?php echo $css?>bootstrap.css" />
   <link rel="stylesheet" href="<?php echo $css?>font-awesome.min.css" />
   <link rel="stylesheet" href="<?php echo $css?>jquery-ui-1.9.1.custom.css" />
   <link rel="stylesheet" href="<?php echo $css?>jquery.selectBoxIt.?css" />
   <link rel="stylesheet" href="<?php echo $css?>backend.?css" />

   <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->
   
   <script src="<?php echo $js?>html5shiv.min.js"></script>
   <script src="<?php echo $js?>respond.min.js"></script> <!-- -->
</head>
<body>