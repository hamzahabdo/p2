$(function (){

    'use strict';
    // dashbaord
    //input seach
      $(".search-inp").hide();
      $(".ser").click(function(){
        $(".search-inp").fadeToggle();
      });
    //input seach
    /*
    $(".ser").click(function(){
    $.ajax({
        type: "get",
        url: "members.php",
        data: {
         do : "view"
        },
        dataType: "html",
        success: function (response) {
                $(".main-table").slideUp();
                $(".main-table").html(response);
                $(".main-table").slideDown(1000);
        }
    });
});*/
    //==============
    $('.toggle-info').click(function(){
        $(this).toggleClass('selected').parent().next('.panal-body').fadeToggle(100);
        if($(this).hasClass('selected')){
            $(this).html("<i class='fa fa-plus fa-lg'></i>");
        } else {
            $(this).html("<i class='fa fa-minus fa-lg'></i>");
        }
    });
     
    //trigger the selectboxit

    //trigger the selectboxit
    $('select').selectBoxIt({
        autoWidth : false 
    });

    $('[placeholder]').focus(function(){
        $(this).attr('data-text',$(this).attr('placeholder'));
        $(this).attr('placeholder','');
    }).blur(function (){
        $(this).attr('placeholder',$(this).attr('data-text'));

    });   //end focus

    // Add Asterisk on required field 
$('input').each(function(){
    if($(this).attr('required') === 'required'){
        $(this).after('<span  class="asterisk">*</span>').addClass('mem-input');
    }
});//Add Asterisk on required field 

//convert password to text on hover
 var passfield = $('.password');
$('.show-pass').hover(function(){
    passfield.attr('type','text');
},function(){
    passfield.attr('type','password');
});//convert password to text on hover

//confirmation Massage on Button
$('.confirm').click(function(){
    return confirm('Are You sure?!');
});//confirmation Massage on Button

//catagry option view
$('.cat h3').click(function(){
    $(this).next('.full-view').fadeToggle(200);
});

$('.option span').click(function(){
    $(this).addClass('active').siblings('span').removeClass('active');

    if($(this).data('view') === 'full'){
        $('.cat .full-view').fadeIn(200);
    } else {
        $('.cat .full-view').fadeOut(200);
    }
});
$('.child-link').hover(function(){
  $(this).find('.show-delete').fadeIn(400);
},function(){
    $(this).find('.show-delete').fadeOut(400);
}
);

});// end fun

