<?php
/*
===========================
  - Manage Items page
  - You add | Edit | Delete members from here
===========================
*/
ob_start(); //output Buffering Start  //ob_gzhandler
session_start();
$pageTitle = 'Items';
//==================Start Manage Page======================================
function manage(){
  $query = '';
      if(isset($_GET['page']) && $_GET['page'] == 'pending'){
      $query = "AND regstate = 0";
      }
      global $con;
      $stmt = $con->prepare("SELECT 
                   items.* , catagories.Name AS Cat_Name , users.Username 
                  FROM 
                   items
                  INNER JOIN catagories ON 
                  catagories.ID = items.Cat_ID
                  INNER JOIN users ON 
                  users.userId = items.Member_ID
                  ORDER BY item_ID DESC");
      $stmt->execute();

      $items = $stmt->fetchAll();
      
      ?>
          <h1 class="text-center">Manage Items</h1>
          <div class='container'>
            <?php if(! empty($items)){ ?>
          <div class="table-responsive">
            <table class="main-table text-center table table-bordered">
             <tr>
              <td>#ID</td>
              <td>Nmae</td>
              <td>Description</td>
              <td>Price</td>
              <td>Add_Date</td>
              <td>Made In</td>
              <td>Catagory</td>
              <td>Username</td>
              <td>Control</td>
             </tr>

             <?php
             foreach($items as $item){
               echo "<tr>";
                  echo "<td>". $item['item_ID'] . "</td>"; 
                  echo "<td>". $item['Name'] ."</td>";
                  echo "<td>". $item['Description'] . "</td>";
                  echo "<td>". $item['Price'] . "</td>";
                  echo "<td>". $item['Add_Date'] . "</td>";
                  echo "<td>". $item['Country_Made'] . "</td>";
                  echo "<td>". $item['Cat_Name'] . "</td>";
                  echo "<td>". $item['Username'] . "</td>";
                  echo "<td>
                       <a href='items.php?do=Edit&item_ID=".$item['item_ID']. "'class='btn btn-success'><i class='fa fa-edit'></i> Edit</a>
                       <a href='items.php?do=Delete&item_ID=".$item['item_ID']. "' class='btn btn-danger confirm'><i class='fa fa-close'></i> Delete</a>";
                       if($item['Approve'] == 0){
                        echo "<a href='items.php?do=Approve&item_ID=".$item['item_ID']. "' class='btn btn-info activate'><i class='fa fa-check'></i> Approve</a>";
                       }
                      echo "</td>"; 
               echo "</tr>";
             }
             ?>
             
            </table>
          </div>
          <a href="items.php?do=Add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> New Item</a>
            <?php } else{
              echo "<div class='empty-rec'>There Is No Items To Show</div>";
              echo '<a href="items.php?do=Add" class="btn btn-primary btn-lg"><i class="fa fa-plus"></i> New Item</a>';
            } ?>
        </div>
             
            <?php 
}
//==================End Manage Page======================================
//==================Start Insert Page======================================
function insert(){
  
  echo "<h1 class='text-center'>Insert Item</h1>";
  echo "<div class='container'>";
    //get variable from the form
    $name = $_POST['name'];
    $desc = $_POST['description'];
    $price = $_POST['price'];
    $country = $_POST['country'];
    $status = $_POST['status'];
    $member = $_POST['member'];
    $cat = $_POST['catagory'];
    $tags = $_POST['tags'];
  
    //validate the form
    $formError = array();

    if(empty($name)){
      $formError[] = 'Name cant be <strong>empty</strong>';
    }
    if(empty($desc)){
      $formError[] = 'Desc cant be <strong>empty</strong>';
    }
    if(empty($price)){
      $formError[] = 'Price cant be <strong>empty</strong>';
    }
    if(empty($country)){
      $formError[] = 'Country cant be <strong>empty</strong>';
    }
    if($status == 0){
       $formError[] = 'You Must Select <strong>Status</strong>';
     }
     if($member == 0){
       $formError[] = 'You Must Select <strong>Member</strong>';
     }
     if($cat == 0){
       $formError[] = 'You Must Select <strong>Catagory</strong>';
     }

    foreach($formError as  $error){
      echo "<div class='alert alert-danger'>".$error . "</div><br>";
    }
   
    if(empty($formError)){
      global $con;
      $stmt = $con ->prepare("INSERT INTO
           items(Name,Description,Price, Country_Made,Status,Add_Date,Member_ID,Cat_ID,tags)
           VALUES(:zname, :zdesc, :zprice, :zcountry, :zstatus, now() ,:zmember ,:zcat ,:ztags)");
                 
                 $stmt->execute(array(
                            'zname' => $name,
                            'zdesc' => $desc,
                            'zprice' => $price,
                            'zcountry' => $country,
                            'zstatus' => $status,
                            'zmember' => $member,
                            'zcat' => $cat,
                            'ztags' => $tags
                          ));
                      $theMsg= "<div class='alert alert-success'>".$stmt->rowCount() . ' record inserted</div>';
                      redirectHome($theMsg,'back');
                                
    }//end if
    //update DB with these data=====================
    
    
    echo "</div>";
}
//==================End Insert Page======================================
//==================Start ADD Page======================================
function Add(){?>
    <h1 class="text-center">Add New Item</h1>
      <div class="container">
         <form class="form-horizontal" action="?do=insert" method="POST">
          <!-- start name field-->
           <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Name</label>
             <div class="col-sm-10 col-md-4">
              <input type="text" 
                     name="name"  
                     class="form-control" 
                     required = 'required' 
                     placeholder="Name of the Item"/>
             </div>
          </div>
          <!-- end name field-->
          <!-- start Description field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Description</label>
             <div class="col-sm-10 col-md-4">
              <input type="text" 
                     name="description"  
                     class="form-control" 
                     required = 'required' 
                     placeholder="Description of the Item"/>
             </div>
          </div>
          <!-- end Description field-->
          <!-- start Price field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Price</label>
             <div class="col-sm-10 col-md-4">
              <input type="text" 
                     name="price"  
                     class="form-control"
                     required = 'required'  
                     placeholder="Price of the Item"/>
             </div>
          </div>
          <!-- end Price field-->
          <!-- start Country field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Country</label>
             <div class="col-sm-10 col-md-4">
              <input type="text" 
                     name="country"  
                     class="form-control"
                     required = 'required'  
                     placeholder="Country of Made"/>
             </div>
          </div>
          <!-- end Country field-->
          <!-- start Status field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Status</label>
             <div class="col-sm-10 col-md-4">
              <select name="status">
                  <option value="0">....</option>
                  <option value="1">New</option>
                  <option value="2">Like New</option>
                  <option value="3">Used</option>
                  <option value="4">Old</option>
              </select>
             </div>
          </div>
          <!-- end Status field-->
          <!-- start Members field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Member</label>
             <div class="col-sm-10 col-md-4">
              <select name="member">
                  <option value="0">....</option>
                  <?php global $con;
                  $users = getAllFrom("*" ,"users" ,"", "" ,"userId" ,"");
                   foreach($users as $user){
                       echo "<option value='".$user['userId']."'>".$user['Username']."</option>";
                   }
                  ?>
              </select>
             </div>
          </div>
          <!-- end Members field-->
          <!-- start Catagory field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Catagory</label>
             <div class="col-sm-10 col-md-4">
              <select name="catagory">
                  <option value="0">....</option>
                  <?php global $con;
                  $cats = getAllFrom("*" ,"catagories" ,"WHERE parent = 0", "" ,"ID" ,"");
                   foreach($cats as $cat){
                       echo "<option value='".$cat['ID']."'>".$cat['Name']."</option>";
                       $childCats = getAllFrom("*" ,"catagories" ,"WHERE parent = {$cat['ID']}", "" ,"ID" ,"");
                       foreach($childCats as $child){
                        echo "<option value='".$child['ID']."'>-- ".$child['Name']."</option>";
                       }
                   }
                  ?>
              </select>
             </div>
          </div>
          <!-- end Catagory field-->
           <!-- start Tags field-->
           <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Tags</label>
             <div class="col-sm-10 col-md-4">
              <input type="text" 
                     name="tags"  
                     class="form-control"
                     placeholder="Separate Tags With Comma (,)"/>
             </div>
          </div>
          <!-- end Tags field-->

           <!-- start submit field-->
           <div class="form-group">
             <div class="col-sm-offset-2 col-sm-10">
              <input type="submit" value="Add Item" class="btn btn-primary btn-sm"/>
             </div>
          </div>
          <!-- end submit field-->
          
         </form>
      </div>
<!-- //==end Add page========================================================================-->
  <?php
  }
  // end Add page
  //==========================================================================

   //Start Edit page
  //==========================================================================
  function Edit($item_ID){
    global $con;
    $stmt = $con ->prepare("SELECT * FROM items WHERE item_iD = ?");
                   $stmt->execute(array($item_ID));
                   // fetch data
                   $item = $stmt->fetch();
                   // the row count
                   $count = $stmt -> rowCount();
                   //if there is id show form  
    if($count > 0){?>   
      <h1 class="text-center">Edit Item</h1>
      <div class="container">
         <form class="form-horizontal" action="?do=Update" method="POST">
         <input type="hidden" name="item_ID" value='<?php echo $item_ID ?>'/>
          <!-- start name field-->
           <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Name</label>
             <div class="col-sm-10 col-md-4">
              <input type="text" 
                     name="name"  
                     class="form-control" 
                     required = 'required' 
                     placeholder="Name of the Item"
                     value="<?php echo $item['Name'];?>"
                     />
             </div>
          </div>
          <!-- end name field-->
          <!-- start Description field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Description</label>
             <div class="col-sm-10 col-md-4">
              <input type="text" 
                     name="description"  
                     class="form-control" 
                     required = 'required' 
                     placeholder="Description of the Item"
                     value="<?php echo $item['Description'];?>"
                     />
             </div>
          </div>
          <!-- end Description field-->
          <!-- start Price field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Price</label>
             <div class="col-sm-10 col-md-4">
              <input type="text" 
                     name="price"  
                     class="form-control"
                     required = 'required'  
                     placeholder="Price of the Item"
                     value="<?php echo $item['Price'];?>"
                     />
             </div>
          </div>
          <!-- end Price field-->
          <!-- start Country field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Country</label>
             <div class="col-sm-10 col-md-4">
              <input type="text" 
                     name="country"  
                     class="form-control"
                     required = 'required'  
                     placeholder="Country of Made"
                     value="<?php echo $item['Country_Made'];?>"
                     />
             </div>
          </div>
          <!-- end Country field-->
          <!-- start Status field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Status</label>
             <div class="col-sm-10 col-md-4">
              <select name="status">
                  <option value="1" <?php if($item['Status'] == 1){echo 'selected';}?>>New</option>
                  <option value="2" <?php if($item['Status'] == 2){echo 'selected';}?>>Like New</option>
                  <option value="3" <?php if($item['Status'] == 3){echo 'selected';}?>>Used</option>
                  <option value="4" <?php if($item['Status'] == 4){echo 'selected';}?>>Old</option>
              </select>
             </div>
          </div>
          <!-- end Status field-->
          <!-- start Members field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Member</label>
             <div class="col-sm-10 col-md-4">
              <select name="member">
                  <?php global $con;
                   $stmt = $con ->prepare("SELECT * FROM users");
                   $stmt ->execute();
                   $users = $stmt->fetchAll();
                   foreach($users as $user){
                       echo "<option value='".$user['userId']."'";
                       if($item['Member_ID'] == $user['userId']){ echo "selected";}
                       echo ">".$user['Username']."</option>";
                   }
                  ?>
              </select>
             </div>
          </div>
          <!-- end Members field-->
          <!-- start Catagory field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Catagory</label>
             <div class="col-sm-10 col-md-4">
              <select name="catagory">
                  <?php global $con;
                   $stmt = $con ->prepare("SELECT * FROM catagories");
                   $stmt ->execute();
                   $cats = $stmt->fetchAll();
                   foreach($cats as $cat){
                       echo "<option value='".$cat['ID']."'";
                       if($item['Cat_ID'] == $cat['ID']){ echo "selected";}
                       echo ">".$cat['Name']."</option>";
                   }
                  ?>
              </select>
             </div>
          </div>
          <!-- end Catagory field-->
           <!-- start Tags field-->
           <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Tags</label>
             <div class="col-sm-10 col-md-4">
              <input type="text" 
                     name="tags"  
                     class="form-control"
                     placeholder="Separate Tags With Comma (,)"
                     value="<?php echo $item['tags'];?>"
                     />
             </div>
          </div>
          <!-- end Tags field-->

           <!-- start submit field-->
           <div class="form-group">
             <div class="col-sm-offset-2 col-sm-10">
              <input type="submit" value="Saves Item" class="btn btn-primary btn-sm"/>
             </div>
          </div>
          <!-- end submit field-->
          
         </form>
    <!---------- Comments-------------------------------------------------->
      <?php
        global $con;
        $stmt = $con->prepare("SELECT comments.*,items.Name,users.Username 
                FROM comments
                INNER JOIN items
                ON items.item_ID = comments.item_ID
                INNER JOIN users
                ON users.userId = comments.user_Id
                WHERE comments.item_ID = ?");
        $stmt->execute(array($item_ID));
  
        $rows = $stmt->fetchAll();
        if(!empty($rows)){
        ?>
            <h1 class="text-center">Manage [ <?php echo $item['Name'];?> ] Comments</h1>
            <div class="table-responsive">
              <table class="main-table text-center table table-bordered">
               <tr>
                <td>#ID</td>
                <td>Comment</td>
                <td>Item Name</td>
                <td>User Name</td>
                <td>Add Date</td>
                <td>Control</td>
               </tr>
  
               <?php
               foreach($rows as $row){
                 echo "<tr>";
                    echo "<td>". $row['C_id'] . "</td>"; 
                    echo "<td>". $row['Comment'] ."</td>";
                    echo "<td>". $row['Name'] . "</td>";
                    echo "<td>". $row['Username'] . "</td>";
                    echo "<td>". $row['Comment_Date'] . "</td>";
                    echo "<td>
                         <a href='comments.php?do=Edit&C_id=".$row['C_id']. "'class='btn btn-success'><i class='fa fa-edit'></i> Edit</a>
                         <a href='comments.php?do=Delete&C_id=".$row['C_id']. "' class='btn btn-danger confirm'><i class='fa fa-close'></i> Delete</a>";
                         if($row['Status'] == 0){
                          echo "<a href='comments.php?do=Approve&C_id=".$row['C_id']. "' class='btn btn-info activate'><i class='fa fa-check'></i> Approve</a>";
                         }
                        echo "</td>"; 
                 echo "</tr>";
               }
               ?>
               
              </table>
            </div>
              <?php } ?> 
    <!---------- Comments--------------------------------------------------->
      </div>       
<?php
      } // end if
      else {
      echo "<div class='container'>";
      $theMsg= "<div class='alert alert-dager'>No Such As Id</div>"; 
      redirectHome($theMsg);
      echo "</div>";
    }
}
// end Edit page
//==========================================================================

// start  Update Page
function Update(){
  echo "<div class='container'>";
  $item_ID = $_POST['item_ID']; 
  $name = $_POST['name'];
  $desc = $_POST['description'];
  $price = $_POST['price'];
  $country = $_POST['country'];
  $status = $_POST['status'];
  $member = $_POST['member'];
  $cat = $_POST['catagory'];
  $tags = $_POST['tags'];

  //validate the form
  $formError = array();

  if(empty($name)){
    $formError[] = 'Name cant be <strong>empty</strong>';
  }
  if(empty($desc)){
    $formError[] = 'Desc cant be <strong>empty</strong>';
  }
  if(empty($price)){
    $formError[] = 'Price cant be <strong>empty</strong>';
  }
  if(empty($country)){
    $formError[] = 'Country cant be <strong>empty</strong>';
  }
  if($status == 0){
     $formError[] = 'You Must Select <strong>Status</strong>';
   }
   if($member == 0){
     $formError[] = 'You Must Select <strong>Member</strong>';
   }
   if($cat == 0){
     $formError[] = 'You Must Select <strong>Catagory</strong>';
   }

  foreach($formError as  $error){
    echo "<div class='alert alert-danger'>".$error . "</div><br>";
  }
 
  if(empty($formError)){
    
    global $con;
  $stmt = $con ->prepare("UPDATE items 
            SET 
            Name = ?,Description = ? , Price = ? , Country_Made = ? , Status = ? , Member_ID = ? , Cat_ID = ?, tags = ?
                            WHERE item_ID = ? LIMIT 1");
                       $stmt->execute(array($name,$desc,$price,$country,$status,$member,$cat,$tags,$item_ID));
                       echo "<div class='container'>";
                       $theMsg= "<div class='alert alert-success'>".$stmt->rowCount() . ' record updated</div>';
                       redirectHome($theMsg,'back');
                       echo "</div>";
                              
  }//end if
  //update DB with these data=====================
  echo "</div>";
 }//end Update
  //===============================================================================

//=========Start delete Page====================================================
function delete(){
  echo "<h1 class='text-center'>Delete Item</h1> <div class='container'>";
                  $item_ID= isset($_GET['item_ID']) && is_numeric($_GET['item_ID']) ? intval($_GET['item_ID']) : 0 ;
                 $check = checkItem("item_ID","items",$item_ID);
                 //if there is id show form   
                 if($check > 0){ 
                   global $con;
                  $stmt = $con ->prepare("DELETE FROM items WHERE item_ID = :zid ");
                  $stmt ->bindParam(':zid',$item_ID);
                  $stmt ->execute();
                  $theMsg= "<div class='alert alert-success'>".$stmt->rowCount() . ' record Deleted</div>';
                  redirectHome($theMsg,'back');
                 }
                 else { $theMsg= "<div class='alert alert-danger'>This is ID not Exist</div>";
                  redirectHome($theMsg);
                } 
                 echo "</div>";
}
//===========End delete Page===========================================

   //===========Start Approve Page===========================================
   function approve(){
    echo "<h1 class='text-center'>Approve Ads</h1> <div class='container'>";
    $item_ID= isset($_GET['item_ID']) && is_numeric($_GET['item_ID']) ? intval($_GET['item_ID']) : 0 ;
    $check = checkItem("item_ID","items",$item_ID);
    if($check > 0){ 
     global $con;
     $stmt = $con ->prepare("UPDATE items SET Approve = 1 WHERE item_ID = ? LIMIT 1");
     $stmt->execute(array($item_ID));
     
    $theMsg= "<div class='alert alert-success'>".$stmt->rowCount() . ' Item Approved</div>';
     redirectHome($theMsg,'back');
    }  else {
      $theMsg= "<div class='alert alert-danger'>This is ID not Exist</div>";
      redirectHome($theMsg);
    } 
    echo "</div>";
  }
  //===========End Approve Page===========================================

if (isset($_SESSION['username'])){
    include 'init.php';
   
    $do = isset($_GET['do']) ? $_GET['do'] : 'Manage';
    switch($do){
      //======Manage===========================================
      case 'Manage': manage();break;// end manage
      case 'Add': Add(); break;// end Add
      case 'insert':
                  if ($_SERVER['REQUEST_METHOD'] == 'POST'){
                    insert();
                } else{
                  echo "<div class='container'>";
                  $theMsg = '<div class="alert alert-danger">Sorry You can\'t Browse This Page Directly</div>';
                  redirectHome($theMsg);
                  echo "</div>";
                };
                  break;// end insert
      case 'Edit': 
                  $item_ID= isset($_GET['item_ID']) && is_numeric($_GET['item_ID']) ? intval($_GET['item_ID']) : 0 ;
                   Edit($item_ID);
                  break;// end Edit
      case 'Update': 
                  echo "<h1 class='text-center'>Update Item</h1>";
                  if ($_SERVER['REQUEST_METHOD'] == 'POST'){
                    Update();

                    } else{
                      echo "<div class='container'>";
                      $theMsg = '<div class="alert alert-danger">Sorry You can\'t Browse This Page Directly</div>';
                            redirectHome($theMsg);
                            echo "</div>";
                    }
      break;// end update
      case 'Delete':  delete();break;// end Delete
      case 'Approve':approve(); break;// end Approve
      default : echo 'welcome to Default page';
  };

    include $tpl.'footer.php';
} else {
    header('location:index.php');
  exit();
}
ob_end_flush();
?>