<?php
ob_start(); //output Buffering Start  //ob_gzhandler
session_start();
if(isset($_SESSION['username'])){
    $pageTitle = 'Dashboard';
   include 'init.php';
  //header("Location: dashboard.php"); //redirect 
  //echo 'Welcome' . $_SESSION['username'].'<br>'.$_SESSION['ID'];
  // start dashboard

  ?>
<div class="container home-stats text-center">
 <h1>Dashboard</h1>
 <div class="row">
   <div class="col-md-3">
    <div class="stat st-members">
       <i class="fa fa-users"></i>
        <div class='info'>
        Total Members
        <span><a href="members.php"><?php echo countItems('userId','users')?></a></span>
        </div>
    </div>
   </div>
   <div class="col-md-3">
    <div class="stat st-pendings">
    <i class="fa fa-user-plus"></i>
        <div class='info'>
        Pending Members
    <span><a href="members.php?do=Manage&page=pending"><?php echo checkItem('regstate','users', 0)?></a></span></div>
        </div>
   </div>
   <div class="col-md-3">
    <div class="stat st-items">
       <i class="fa fa-tag"></i>
        <div class="info">
        Total Items
        <span><a href="items.php"><?php echo countItems('item_ID','items')?></a></span>    
        </div>
    </div>
   </div>
   <div class="col-md-3">
    <div class="stat st-comments">
    <i class="fa fa-comments"></i>
        <div class="info">
        Total Comments
         <span><a href="comments.php"><?php echo countItems('C_id','comments')?></a></span>
        </div>
    
    </div>
   </div>
 </div>
</div>

<div class="container latest">
    <div class="row">
      <div class="col-sm-6">
          <div class="panel panel-default">
           <div class="panal-heading">
           <?php $latest = 5; ?>
             <i class="fa fa-users"></i> Latest <?php echo $latest; ?> registered Users
                <span class="toggle-info pull-right">
                  <i class="fa fa-minus fa-lg"></i>
                </span>
           </div>   
           <div class="panal-body">
             <ul class="list-unstyled latest-users">
             <?php
             $theLatest = getLatest('*','users','userId',$latest);
             if(!empty($theLatest)){
             foreach($theLatest as $row){
               echo "<li>".$row['Username']."<a href='members.php?do=Edit&userId=".$row['userId']."'>
               <span class='btn btn-success pull-right'>
               <i class='fa fa-edit'></i>
               Edit";
               if($row['regstate'] == 0){
                echo "<a href='members.php?do=Activate&userId=".$row['userId']. "' class='btn btn-info pull-right activate'><i class='fa fa-check'></i> Activate</a>";
               }
               echo "</span></a></li>";
             } 
            }else {
              echo "<div class='empty-rec'>There Is No Users To Show</div>";
            }
             
             ?>
             </ul>
           </div>   
          </div>
      </div>
      <div class="col-sm-6">
          <div class="panel panel-default">
           <div class="panal-heading">
             <i class="fa fa-tag"></i> Latest <?php echo $latest; ?> Items
             <span class="toggle-info pull-right">
                  <i class="fa fa-minus fa-lg"></i>
                </span>
           </div>   
           <div class="panal-body">
              <ul class="list-unstyled latest-users">
                <?php
                $num_items = getLatest('*','items','item_ID',$latest);
                if(! empty($num_items)){
                foreach($num_items as $item){
                  echo "<li>".$item['Name']."<a href='items.php?do=Edit&item_ID=".$item['item_ID']."'>
                  <span class='btn btn-success pull-right'>
                  <i class='fa fa-edit'></i>
                  Edit";
                  if($item['Approve'] == 0){
                    echo "<a href='items.php?do=Approve&item_ID=".$item['item_ID']. "' class='btn btn-info pull-right activate'><i class='fa fa-check'></i> Approve</a>";
                  }
                  echo "</span></a></li>";
            
                }//end foreach
              } else {
                echo "<div class='empty-rec'>There Is No Items To Show</div>";
              }
                ?>
                </ul>
           </div>   
          </div>
      </div>
    </div> <!--End row 1 -->
    <div class="row">
      <div class="col-sm-6 latest-comm-col">
          <div class="panel panel-default">
           <div class="panal-heading">
           <?php $latest = 5; ?>
             <i class="fa fa-comments"></i> Latest <?php echo $latest; ?> Comments
                <span class="toggle-info pull-right">
                  <i class="fa fa-minus fa-lg"></i>
                </span>
           </div>   
           <div class="panal-body">
             <div class="latest-comments">
           <?php 
           //global $con;
              $stmt = $con->prepare("SELECT comments.*,users.Username 
                      FROM comments
                      INNER JOIN users
                      ON users.userId = comments.user_Id
                      ORDER BY C_id DESC
                      LIMIT $latest");
              $stmt->execute();
              $comments = $stmt->fetchAll();
              if(! empty($num_items)){
              foreach($comments as $comment){
                echo '<div class="comment-box">';
                 echo "<span class='member-n' >".$comment['Username']."</span>";
                 echo "<p class='member-c'>".$comment['Comment']."</p>";
                echo "</div>";
              }
            } else {
              echo "<div class='empty-rec'>There Is No Commends To Show</div>";
            }
              ?>
              </div>
           </div>   
          </div>
      </div>
      <div class="col-sm-6 ">
          <div class="panel panel-default">
           <div class="panal-heading">
             <i class="fa fa-tag"></i> Latest <?php echo $latest; ?> Items
             <span class="toggle-info pull-right">
                  <i class="fa fa-minus fa-lg"></i>
                </span>
           </div>   
           <div class="panal-body">
           <ul class="list-unstyled latest-users">
             <?php
             $theLatest = getLatest('*','users','userId',$latest);
             foreach($theLatest as $row){
               echo "<li>".$row['Username']."<a href='members.php?do=Edit&userId=".$row['userId']."'>
               <span class='btn btn-success pull-right'>
               <i class='fa fa-edit'></i>
               Edit";
               if($row['regstate'] == 0){
                echo "<a href='members.php?do=Activate&userId=".$row['userId']. "' class='btn btn-info pull-right activate'><i class='fa fa-check'></i> Activate</a>";
               }
               echo "</span></a></li>";
               
             }
             ?>
             </ul>
           </div>   
          </div>
      </div> <!--End row 1 -->
    <div>


</div>
  <?php
  // End dashboard
  include $tpl .'footer.php';
  
}
else{
    //echo 'you not authrizied';
    header("Location: index.php"); //redirect 
    exit();

}
ob_end_flush();
?>