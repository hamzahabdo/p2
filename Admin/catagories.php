<?php
/*
===========================
  - Catagory page
  - You add | Edit | Delete members from here
===========================
*/
ob_start(); //output Buffering Start  //ob_gzhandler
session_start();
$pageTitle = 'Catagories';

function manage(){
  
  $sort = 'ASC';
  $sort_array = array('DESC','ASC');
  if(isset($_GET['sort']) && in_array($_GET['sort'],$sort_array)){
    $sort = 'DESC';
  }
  $cats = getAllFrom("*" ,"catagories" ,"WHERE parent = 0" , "" ,"Ordering" ,$sort);
  /*
  global $con;
  $stmt2 = $con->prepare("SELECT * FROM catagories ORDER BY Ordering $sort");
  $stmt2->execute();

  $cats = $stmt2->fetchAll();
  */
  ?>
  <h1 class="text-center">Manage Catagories</h1>
  <div class='container catagories'>
  <?php if(! empty($cats)){ ?>
    <div class="panel panel-default">
      <div class="panel-heading"><i class='fa fa-edit'></i> Manage Catagories
        <div class="option pull-right">
          <i class='fa fa-sort'></i> Ordering:[ 
          <a href="?sort-ASC" class="<?php if($sort=='ASC'){echo 'active';}?>">ASC</a> | 
          <a href="?sort=DESC" class="<?php if($sort=='DESC'){echo 'active';}?>">DESC</a> ]
          <i class='fa fa-eye'></i> View:[ 
          <span data-view="full">Full</span> | 
          <span data-view="classic">Classic</span> ]
        </div>
        
      </div>
        <div class="panel-body">
           <?php
          foreach($cats as $cat){
            echo "<div class='cat'>";
            echo "<div class='hidden-buttons'>";
              echo "<a href='?do=Edit&ID=".$cat['ID']."' class='btn-xs- btn-primary'><i class='fa fa-edit'></i>Edit</a>";
              echo "<a href='catagories.php?do=Delete&ID=".$cat['ID']."' class='confirm btn-xs- btn-danger'><i class='fa fa-close'></i>Delete</a>";
            echo "</div>";
              echo  "<h3>".$cat['Name'] . "</h3>";
              echo "<div class='full-view'>";
              echo  "<p>";
                  if($cat['Description']==''){echo "This Catagory Has no Description";}
                    else{ echo $cat['Description'] ;}  
                  echo "</p>";
                  echo  "<span>Ordering is ".$cat['Ordering'] . "</span>";
                  //echo  "<span>Visibility is " .$cat['Visibility'] . "</span>";
                  if($cat['Visibility'] == 1){ echo "<span class='visibility vca'><i class='fa fa-eye'></i> Hidden</span>";}
                  if($cat['Allow_Comment'] == 1){ echo "<span class='Commenting vca'><i class='fa fa-close'></i> Comments disabled</span>";}
                  if($cat['Allow_Ads'] == 1){ echo "<span class='adverts vca'><i class='fa fa-close'></i> Ads Disabled</span>";}
              echo "</div>";//full-view
              $childs = getAllFrom("*" ,"catagories" ,"WHERE parent = {$cat['ID']}" ,"" ,"ID" ,"");
              if(! empty($childs)){
                echo "<h4 class='child-head'>Sub Catagories</h4>";
                echo "<ul class='list-unstyled child-cats'>";
                  foreach($childs as $c){
                    echo "<li class='child-link'>
                    <a href='?do=Edit&ID=".$c['ID']."' >".$c['Name'] . "</a>
                    <a href='catagories.php?do=Delete&ID=".$c['ID']."' class='show-delete confirm'>Delete</a>
                    </li>";
                  }
                  echo "</ul>";
                }
              echo "</div>";// cat
            echo "<hr>";
           
          }
           ?>
        </div>
    </div>
    <?php } else {
          echo "<div class='empty-rec'>There Is No Catagories To Show</div>";
        } ?>
    <a href="catagories.php?do=Add" class="add-catagory btn btn-primary"><i class="fa fa-plus"></i> New Catagory</a>
  </div>
          <?php
  
}
//====================Start Add Page======================================
function Add(){?>
    <h1 class="text-center">Add New Catagory</h1>
      <div class="container">
         <form class="form-horizontal" action="?do=insert" method="POST">
          <!-- start username field-->
           <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">name</label>
             <div class="col-sm-10 col-md-4">
              <input type="text" name="name"  class="form-control" autocompelte="off" required = 'required' placeholder="name of the Catagory"/>
             </div>
          </div>
          <!-- end name field-->
          <!-- start description field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Description</label>
             <div class="col-sm-10 col-md-4">
              <input type="text" name="description"  class="form-control" autocompelte="off"  placeholder="Discriibe of the Catagory"/>
             </div>
          </div>
          <!-- end description  field-->
          <!-- start Ordering field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Ordering</label>
             <div class="col-sm-10 col-md-4">
              <input type="text" name="ordering"  class="form-control" autocompelte="off"  placeholder="Number of Ordering arrange"/>
             </div>
          </div>
          <!-- end Ordering field-->
          <!-- Start Catagory Type -->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Parent?</label>
             <div class="col-sm-10 col-md-4">
               <select name="parent">
                  <option value="0">None</option>
                  <?php 
                   $allCats = getAllFrom("*" ,"catagories" ,"WHERE parent = 0" ,"" ,"ID","");
                   foreach($allCats as $cat){
                     echo "<option value='".$cat['ID']."'>".$cat['Name']."</option>";
                   }
                  ?>
               </select>
             </div>
          </div>
          <!-- End Catagory Type -->
          <!-- start Visibility field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Visible</label>
             <div class="col-sm-10 col-md-4">
             <div>
                <input id="vis-yes" type="radio" name="visibility" value="0" checked />
                <label for="vis-yes">Yes<label>
              </div>
              <div>
                <input id="vis-no" type="radio" name="visibility" value="1" />
                <label for="vis-no">No<label>
              </div>
             </div>
          </div>
          <!-- end Visibility field-->
          <!-- start Commenting field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Allow Commenting</label>
             <div class="col-sm-10 col-md-4">
             <div>
                <input id="com-yes" type="radio" name="commenting" value="0" checked />
                <label for="com-yes">Yes<label>
              </div>
              <div>
                <input id="com-no" type="radio" name="commenting" value="1" />
                <label for="com-no">No<label>
              </div>
             </div>
          </div>
          <!-- end Commenting field-->
           <!-- start Ads field-->
           <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Allow Ads</label>
             <div class="col-sm-10 col-md-4">
             <div>
                <input id="ads-yes" type="radio" name="ads" value="0" checked />
                <label for="ads-yes">Yes<label>
              </div>
              <div>
                <input id="ads-no" type="radio" name="ads" value="1" />
                <label for="ads-no">No<label>
              </div>
             </div>
          </div>
          <!-- end Ads field-->


           <!-- start submit field-->
           <div class="form-group">
             <div class="col-sm-offset-2 col-sm-10">
              <input type="submit" value="Add Catagory" class="btn btn-primary btn-lg"/>
             </div>
          </div>
          <!-- end submit field-->
          
         </form>
      </div>
  <?php
  }
  // end Add page
  //==========================================================================
  //=================Start Insert Page========================================== 
  function insert (){
    echo "<div class='container'>";
         //get variable from the form
         $name = $_POST['name'];
         $desc = $_POST['description'];
         $order = $_POST['ordering'];
         $parent = $_POST['parent'];
         $visible = $_POST['visibility'];
         $comment = $_POST['commenting'];
         $ads = $_POST['ads'];

         //echo $id . $user . $email .$name;
       
         //validate the form
      
           $check = checkItem("name","catagories",$name);
           if($check == 1){
             $theMsg= "<div class='alert alert-danger'>Catagory exist</div>";
                           redirectHome($theMsg,'back');
              
           } else {
             global $con;
           $stmt = $con ->prepare("INSERT INTO
                           catagories(Name, Description, Ordering,parent, Visibility, Allow_Comment,Allow_Ads)
                           VALUES(:zname, :zdesc, :zorder,:zparent, :zvisible,:zcomment , :zads)");
                      
                      $stmt->execute(array(
                                 'zname' => $name,
                                 'zdesc' => $desc,
                                 'zorder' => $order,
                                 'zparent' => $parent,
                                 'zvisible' => $visible,
                                 'zcomment' => $comment,
                                 'zads' => $ads
                               ));
                           $theMsg= "<div class='alert alert-success'>".$stmt->rowCount() . ' record inserted</div>';
                           redirectHome($theMsg,'back');
                   }//end else                  
         
         //update DB with these data=====================
         
         
         echo "</div>";
  }
 //=================End Insert Page========================================== 
 
  //=================Start Edit Page========================================== 
  function Edit($catId){
    //select all data depent on ID
    global $con;
    $stmt = $con ->prepare("SELECT * FROM Catagories WHERE ID = ?");
    $stmt->execute(array($catId));
    // fetch data
    $cat = $stmt->fetch();
    // the row count
    $count = $stmt -> rowCount();
    //if there is id show form  
    //Edit($cat['ID'],$cat['Name'],$cat['Description'],$cat['Ordering'],$cat['Visibility'],$cat['Allow_Comment'],$cat['Allow_Ads']) 
    if($count > 0){
    ?>
    <h1 class="text-center">Update Catagory</h1>
      <div class="container">
         <form class="form-horizontal" action="?do=Update" method="POST">
         <input type="hidden" name="ID" value='<?php echo $cat['ID']; ?>'/>
          <!-- start username field-->
           <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">name</label>
             <div class="col-sm-10 col-md-4">
              <input type="text" name="name" value="<?php echo $cat['Name']?>" class="form-control"  required = 'required' placeholder="name of the Catagory"/>
             </div>
          </div>
          <!-- end name field-->
          <!-- start description field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Description</label>
             <div class="col-sm-10 col-md-4">
              <input type="text" name="description" value="<?php echo $cat['Description']?>"  class="form-control"   placeholder="Discriibe of the Catagory"/>
             </div>
          </div>
          <!-- end description  field-->
          <!-- start Ordering field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Ordering</label>
             <div class="col-sm-10 col-md-4">
              <input type="text" name="ordering" value="<?php echo $cat['Ordering']?>" class="form-control"   placeholder="Number of Ordering arrange"/>
             </div>
          </div>
          <!-- end Ordering field-->
          <!-- Start Catagory Type -->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Parent?</label>
             <div class="col-sm-10 col-md-4">
               <select name="parent">
                  <option value="0">None</option>
                  <?php 
                   $allCats = getAllFrom("*" ,"catagories" ,"WHERE parent = 0" ,"" ,"ID","");
                   foreach($allCats as $c){
                     echo "<option value='".$c['ID']."'";
                     if($cat['parent'] == $c['ID']){echo 'selected';}
                     echo ">".$c['Name']."</option>";
                   }
                  ?>
               </select>
             </div>
          </div>
          <!-- End Catagory Type -->
          <!-- start Visibility field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Visible</label>
             <div class="col-sm-10 col-md-4">
             <div>
                <input id="vis-yes" type="radio" name="visibility" value="0" <?php if($cat['Visibility'] == 0){echo 'checked';}?> />
                <label for="vis-yes">Yes<label>
              </div>
              <div>
                <input id="vis-no" type="radio" name="visibility" value="1" <?php if($cat['Visibility'] == 1){echo 'checked';}?> />
                <label for="vis-no">No<label>
              </div>
             </div>
          </div>
          <!-- end Visibility field-->
          <!-- start Commenting field-->
          <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Allow Commenting</label>
             <div class="col-sm-10 col-md-4">
             <div>
                <input id="com-yes" type="radio" name="commenting" value="0" <?php if($cat['Allow_Comment'] == 0){echo 'checked';}?> />
                <label for="com-yes">Yes<label>
              </div>
              <div>
                <input id="com-no" type="radio" name="commenting" value="1" <?php if($cat['Allow_Comment'] == 1){echo 'checked';}?> />
                <label for="com-no">No<label>
              </div>
             </div>
          </div>
          <!-- end Commenting field-->
           <!-- start Ads field-->
           <div class="form-group form-group-lg">
             <label class="col-sm-2 control-label">Allow Ads</label>
             <div class="col-sm-10 col-md-4">
             <div>
                <input id="ads-yes" type="radio" name="ads" value="0" <?php if($cat['Allow_Ads'] == 0){echo 'checked';}?> />
                <label for="ads-yes">Yes<label>
              </div>
              <div>
                <input id="ads-no" type="radio" name="ads" value="1" <?php if($cat['Allow_Ads'] == 1){echo 'checked';}?>/>
                <label for="ads-no">No<label>
              </div>
             </div>
          </div>
          <!-- end Ads field-->


           <!-- start submit field-->
           <div class="form-group">
             <div class="col-sm-offset-2 col-sm-10">
              <input type="submit" value="Save" class="btn btn-primary btn-lg"/>
             </div>
          </div>
          <!-- end submit field-->
          
         </form>
      </div>
  <?php
    }
  else {         
    echo "<div class='container'>";
    $theMsg= "<div class='alert alert-dager'>no such id</div>"; 
    redirectHome($theMsg);
    echo "</div>";
  }
  }
  //====================end Add page======================================================

  //====================Start Update page======================================================
  function update(){
    echo "<h1 class='text-center'>Update Catagory</h1>";
    echo "<div class='container'>";
      //get variable from the form
      $id = $_POST['ID'];
      $name = $_POST['name'];
      $desc = $_POST['description'];
      $order = $_POST['ordering'];
      $parent = $_POST['parent'];
      $visible = $_POST['visibility'];
      $comment = $_POST['commenting'];
      $ads = $_POST['ads'];

      //validate the form
      
     
      global $con;
        $stmt = $con ->prepare("UPDATE catagories SET Name = ?, Description = ?, Ordering = ?,parent = ?, Visibility = ?, Allow_Comment = ?, Allow_Ads = ? 
                        WHERE ID = ?");
                   $stmt->execute(array($name,$desc,$order,$parent,$visible,$comment,$ads,$id));
                   echo "<div class='container'>";
                   $theMsg= "<div class='alert alert-success'>".$stmt->rowCount() . ' record updated</div>';
                   redirectHome($theMsg,'back');
                   echo "</div>";
             
      //update DB with these data=====================
      
      
      echo "</div>";
  }
  //====================End Update page======================================================
  
//=====================Start Delete Page ===================================
function delete(){
  echo "<h1 class='text-center'>Delete Catagory</h1> <div class='container'>";
          $id= isset($_GET['ID']) && is_numeric($_GET['ID']) ? intval($_GET['ID']) : 0 ;
       
        $check = checkItem("ID","catagories",$id);
        //if there is id show form   
        if($check > 0){ 
          global $con;
          $stmt = $con ->prepare("DELETE FROM catagories WHERE id = :zid ");
          $stmt ->bindParam(':zid',$id);
          $stmt ->execute();
          $theMsg= "<div class='alert alert-success'>".$stmt->rowCount() . ' record Deleted</div>';
          redirectHome($theMsg,'back');
        }
        else { $theMsg= "<div class='alert alert-danger'>This is ID not Exist</div>";
          redirectHome($theMsg);
        } 
        echo "</div>";
}
if (isset($_SESSION['username'])){
    include 'init.php';
   
    $do = isset($_GET['do']) ? $_GET['do'] : 'Manage';
    switch($do){
      //======Manage===========================================
      case 'Manage': manage(); break;// end manage
      case 'Add': Add(); break;// end Add
      case 'insert': 
                  if ($_SERVER['REQUEST_METHOD'] == 'POST'){
                    echo "<h1 class='text-center'>Insert Catagory</h1>";
                    insert();
                  
                } else{
                  echo "<div class='container'>";
                  $theMsg = '<div class="alert alert-danger">Sorry You can\'t Browse This Page Directly</div>';
                  redirectHome($theMsg,'back');
                  echo "</div>";
                }
                        break;// end insert
      case 'Edit': 
                    $catId= isset($_GET['ID']) && is_numeric($_GET['ID']) ? intval($_GET['ID']) : 0 ;
                    Edit($catId);
                  break;// End Edit
      case 'Update': 
                    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
                      update();
                    } else{
                      echo "<div class='container'>";
                      $theMsg = '<div class="alert alert-danger">Sorry You can\'t Browse This Page Directly</div>';
                            redirectHome($theMsg);
                            echo "</div>";
                    }
                    break;// end update
      case 'Delete': delete(); break;// end Delete
      default : echo 'welcome to Default page';
  };

    include $tpl.'footer.php';
} else {
    header('location:index.php');
  exit();
}

ob_end_flush();
?>