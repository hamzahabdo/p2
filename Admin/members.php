<?php
/*
===========================
  - Manage members page
  - You add | Edit | Delete members from here
===========================
*/

ob_start(); //output Buffering Start  //ob_gzhandler
session_start();
$pageTitle = 'Members';
/*
function view(){
  $query = '';
      if(isset($_GET['page']) && $_GET['page'] == 'pending'){
      $query = "AND regstate = 0";
      }
      //if(isset($_GET['m'])){
      global $con;
      $stmt = $con->prepare("SELECT * FROM users WHERE groupid != 1 $query ORDER BY userId DESC");
      $stmt->execute();

      $rows = $stmt->fetchAll();
  ?>
             <tr id="head-t">
              <td>#ID</td>
              <td>Username</td>
              <td>Email</td>
              <td>Fullname</td>
              <td>Registered Date</td>
              <td>Control</td>
             </tr>

             <?php
             foreach($rows as $row){
               echo "<tr>";
                  echo "<td>". $row['userId'] . "</td>"; 
                  echo "<td>". $row['Username'] ."</td>";
                  echo "<td>". $row['email'] . "</td>";
                  echo "<td>". $row['fullname'] . "</td>";
                  echo "<td>". $row['Date'] . "</td>";
                  echo "<td>
                       <a href='members.php?do=Edit&userId=".$row['userId']. "'class='btn btn-success'><i class='fa fa-edit'></i> Edit</a>
                       <a href='members.php?do=Delete&userId=".$row['userId']. "' class='btn btn-danger confirm'><i class='fa fa-close'></i> Delete</a>";
                       if($row['regstate'] == 0){
                        echo "<a href='members.php?do=Activate&userId=".$row['userId']. "' class='btn btn-info activate'><i class='fa fa-check'></i> Activate</a>";
                       }
                      echo "</td>"; 
               echo "</tr>";
             }
             
}*/
//===========Start Manage Page===================================================
function manage(){
  
  $query = '';
      if(isset($_GET['page']) && $_GET['page'] == 'pending'){
      $query = "AND regstate = 0";
      }
      global $con;
      $stmt = $con->prepare("SELECT * FROM users WHERE groupid != 1 $query ORDER BY userId DESC");
      $stmt->execute();

      $rows = $stmt->fetchAll();
      ?> 
          <h1 class="text-center">Manage Member</h1>
          <div class='container'>
          <?phpif(!empty($rows)){ ?>
            <div class="col-xm-10 search-box">
              <form>
            <input type="text" class='form-control search-inp' value="" id="search">
            <span class="ser"><i class="fa fa-search fa-lg"></i></span>
            </form>
          </div>
          <div class="table-responsive">
            <table class="main-table manage-members text-center table table-bordered">
             
            
             <tr id="head-t">
              <td>#ID</td>
              <td>Avatar</td>
              <td>Username</td>
              <td>Email</td>
              <td>Fullname</td>
              <td>Registered Date</td>
              <td>Control</td>
             </tr>
          
             <?php
             foreach($rows as $row){
               echo "<tr>";
                  echo "<td>". $row['userId'] . "</td>"; 
                  echo "<td>";
                      if(!empty($row['avatar'])){
                      echo"<img src='uploads/avatars/". $row['avatar'] . "' alt=''/>"; 
                    } else {
                      echo"<img src='../1.jpg' alt=''/>"; 
                    }
                    echo "</td>";
                  echo "<td>". $row['Username'] ."</td>";
                  echo "<td>". $row['email'] . "</td>";
                  echo "<td>". $row['fullname'] . "</td>";
                  echo "<td>". $row['Date'] . "</td>";
                  echo "<td>
                       <a href='members.php?do=Edit&userId=".$row['userId']. "'class='btn btn-success'><i class='fa fa-edit'></i> Edit</a>
                       <a href='members.php?do=Delete&userId=".$row['userId']. "' class='btn btn-danger confirm'><i class='fa fa-close'></i> Delete</a>";
                       if($row['regstate'] == 0){
                        echo "<a href='members.php?do=Activate&userId=".$row['userId']. "' class='btn btn-info activate'><i class='fa fa-check'></i> Activate</a>";
                       }
                      echo "</td>"; 
               echo "</tr>";
             }
             ?>
             
            </table>
          </div>
          <?php //} else{
            //  echo "<div class='empty-rec'>There Is No Users To Show</div>";
           // } ?>
          <a href="members.php?do=Add" class="btn btn-primary"><i class="fa fa-plus"></i> New Member</a>
          </div>
             
            <?php
}
//===========End Manage Page===================================================

function insert(){
  echo "<div class='container'>";

                //upload variable
                //$avatar = $_FILES['avatar'];
                //print_r($avatar);
                $avatarName = $_FILES['avatar']['name'];
                $avatarSize = $_FILES['avatar']['size'];
                $avatarTmp = $_FILES['avatar']['tmp_name'];
                $avatarType = $_FILES['avatar']['type'];

                //list Of Allowed file Typed To Upload
                $avatarAllowedExtension = array("jpeg","jpg","png","gif");
                $extension = explode(".", $avatarName);
                $avatarExtension = strtolower(end($extension));
                //echo "<br>".$avatarExtension;
               // print_r($avatarExtension);
               
                //get variable from the form
                $user = $_POST['username'];
                $pass = $_POST['password'];
                $email = $_POST['email'];
                $name = $_POST['Full'];

                $hashpass = sha1($_POST['password']);
                //echo $id . $user . $email .$name;
              
                //validate the form
                $formError = array();
      
                if(strlen($user) < 4 || strlen($user) > 20){
                  $formError[] = ' Username cant be less than <strong>4 char and More than 20</strong>';
                }
                if(strlen($user) > 20){
                  $formError[] = 'Username cant be less than <strong>4 char</strong>';
                }
                if(empty($user)){
                  $formError[] = 'Username cant be <strong>empty</strong>';
                }
                if(empty($pass)){
                  $formError[] = 'Password cant be <strong>empty</strong>';
                }
                if(empty($email)){
                  $formError[] = 'email cant be <strong>empty</strong>';
                }
                if(empty($name)){
                  $formError[] = 'fullname cant be <strong>empty</strong>';
                }
                if(!empty($avatarName) && !in_array($avatarExtension,$avatarAllowedExtension)){
                  $formError[] = 'This Extension Not <strong>Allowed</strong>';
                }
                if($avatarSize > 524288){//102400
                  $formError[] = 'The Size Can Not Be Larger Than<strong>512 KB</strong>';
                }
               
                foreach($formError as  $error){
                  echo "<div class='alert alert-danger'>".$error . "</div><br>";
                }
               
                if(empty($formError)){
                  $avatar = rand(0 , 1000000) ."_". $avatarName;
                  move_uploaded_file($avatarTmp, "uploads\avatars\\".$avatar);
                  
                  $check = checkItem("Username","users",$user);
                  if($check == 1){
                    $theMsg= "<div class='alert alert-danger'>user exist</div>";
                                  redirectHome($theMsg,'back');
                     
                  } else {
                    global $con;
                  $stmt = $con ->prepare("INSERT INTO
                                  users(Username,Password,email, fullname,Date,avatar)
                                  VALUES(:zuser, :zpass, :zemail, :zname,now(),:zavatar)");
                             
                             $stmt->execute(array(
                                        'zuser' => $user,
                                        'zpass' => $pass,
                                        'zemail' => $email,
                                        'zname' => $name,
                                        'zavatar' => $avatar
                                      ));
                                  $theMsg= "<div class='alert alert-success'>".$stmt->rowCount() . ' record inserted</div>';
                                  redirectHome($theMsg,'back');
                          }//end else  
                                        
                }//end if
                //update DB with these data=====================
                
                
                echo "</div>";
}

//============start Edit page==================================================
  function Edit($userId){
    global $con;
    $stmt = $con ->prepare("SELECT * FROM users WHERE userId = ? LIMIT 1");
                   $stmt->execute(array($userId));
                   // fetch data
                   $row = $stmt->fetch();
                   // the row count
                   $count = $stmt -> rowCount();
                   //if there is id show form  
                   if($count > 0){?>   
  <h1 class="text-center">Edit Member</h1>
    <div class="container">
       <form class="form-horizontal" action="?do=Update" method="POST" enctype="multipart/form-data">
       <input type="hidden" name="userid" value='<?php echo $userId ?>'/>
        <!-- start username field-->
         <div class="form-group form-group-lg mem-input">
           <label class="col-sm-2 control-label">Username</label>
           <div class="col-sm-10 col-md-4">
            <input type="text" name="username" value='<?php echo $row['Username'] ?>' class="form-control mem-input" autocompelte="off" required = 'required'/>
           </div>
        </div>
        <!-- end username field-->
        <!-- start Password field-->
        <div class="form-group form-group-lg">
           <label class="col-sm-2 control-label">Password</label>
           <div class="col-sm-10 col-md-4">
            <input type="hidden" name="oldPassword" value='<?php echo $row['Password'] ?>'/>
            <input type="Password" name="newPassword" class="form-control" autocompelte="new-password" placeholder="leave plank if you dont want to change"/>
           </div>
        </div>
        <!-- end Password field-->
        <!-- start Email field-->
        <div class="form-group form-group-lg">
           <label class="col-sm-2 control-label">Email</label>
           <div class="col-sm-10 col-md-4">
            <input type="Email" name="email" value='<?php echo  $row['email'] ?>' class="form-control" required="required"/>
           </div>
        </div>
        <!-- end Email field-->
        <!-- start Fullname field-->
        <div class="form-group form-group-lg">
           <label class="col-sm-2 control-label">Fullname</label>
           <div class="col-sm-10 col-md-4">
            <input type="text" name="Full" value='<?php echo $row['fullname'] ?>' class="form-control" required="required"/>
           </div>
        </div>
        <!-- end Fullname field-->
        <!-- start Avatar field-->
        <div class="form-group form-group-lg">
           <label class="col-sm-2 control-label">User Avatar</label>
           <div class="col-sm-10 col-md-4">
            <input type="file" name="avatar" value='<?php echo $row['avatar'] ?>' class="form-control"/>
            
           </div>
        </div>
        <!-- end Avatar field-->
         <!-- start submit field-->
         <div class="form-group">
           <div class="col-sm-offset-2 col-sm-10">
            <input type="submit" value="Save" class="btn btn-primary btn-lg"/>
           </div>
        </div>
        <!-- end submit field-->
        
       </form>
    </div>
<?php
    } // end if
    else {
     echo "<div class='container'>";
     $theMsg= "<div class='alert alert-dager'>no such id</div>"; 
     redirectHome($theMsg);
     echo "</div>";
   }
}
// end Edit page
//==========================================================================
//==============================================================
// start Add page
function Add(){?>
  <h1 class="text-center">Add New Member</h1>
    <div class="container">
       <form class="form-horizontal" action="?do=insert" method="POST" enctype="multipart/form-data">
        <!-- start username field-->
         <div class="form-group form-group-lg">
           <label class="col-sm-2 control-label">Username</label>
           <div class="col-sm-10 col-md-4">
            <input type="text" name="username"  class="form-control" autocompelte="off" required = 'required' placeholder="username to login to shop"/>
           </div>
        </div>
        <!-- end username field-->
        <!-- start Password field-->
        <div class="form-group form-group-lg">
           <label class="col-sm-2 control-label">Password</label>
           <div class="col-sm-10 col-md-4">
            <input type="Password" name="password" class="password form-control" autocompelte="new-password" required="required" placeholder="Passwor must be hard"/>
            <i class="show-pass fa fa-eye fa-2x"></i>
           </div>
        </div>
        <!-- end Password field-->
        <!-- start Email field-->
        <div class="form-group form-group-lg">
           <label class="col-sm-2 control-label">Email</label>
           <div class="col-sm-10 col-md-4">
            <input type="Email" name="email"  class="form-control" required="required" placeholder="Email must be Valid"/>
           </div>
        </div>
        <!-- end Email field-->
        <!-- start Fullname field-->
        <div class="form-group form-group-lg">
           <label class="col-sm-2 control-label">Fullname</label>
           <div class="col-sm-10 col-md-4">
            <input type="text" name="Full"  class="form-control" required="required" placeholder="Fullname appear in prolfile page"/>
           </div>
        </div>
        <!-- end Fullname field-->
        <!-- start Avatar field-->
        <div class="form-group form-group-lg">
           <label class="col-sm-2 control-label">User Avatar</label>
           <div class="col-sm-10 col-md-4">
            <input type="file" name="avatar"  class="form-control" required="required" />
           </div>
        </div>
        <!-- end Avatar field-->
         <!-- start submit field-->
         <div class="form-group">
           <div class="col-sm-offset-2 col-sm-10">
            <input type="submit" value="Add Memeber" class="btn btn-primary btn-lg"/>
           </div>
        </div>
        <!-- end submit field-->
        
       </form>
    </div>
<?php
}
// end Add page
//==========================================================================


// start  Update Page
function Update(){
/*
  $avatarName = $_FILES['avatar']['name'];
  $avatarSize = $_FILES['avatar']['size'];
  $avatarTmp = $_FILES['avatar']['tmp_name'];
  $avatarType = $_FILES['avatar']['type'];

  //list Of Allowed file Typed To Upload
  $avatarAllowedExtension = array("jpeg","jpg","png","gif");
  $extension = explode(".", $avatarName);
  $avatarExtension = strtolower(end($extension));
*/
  echo "<div class='container'>";
  echo $_FILES['avatar']['name'];
  /*
  $id = $_POST['userid'];
          $user = $_POST['username'];
          $email = $_POST['email'];
          $name = $_POST['Full'];
          //password trick=============================
          $pass = empty($_POST['newPassword']) ?  $_POST['oldPassword'] : sha1($_POST['newPassword']) ;

          //validate the form
          $formError = array();

          if(strlen($user) < 4 || strlen($user) > 20){
            $formError[] = '<div class="alert alert-danger"> Username cant be less than <strong>4 char and More than 20</strong></div>';
          }
          
          $check = checkItem("Username","users",$user);
          global $con;
            $stmt = $con ->prepare("SELECT Username FROM users 
                                    WHERE userId != ? AND Username = ? LIMIT 1");
          
                       $stmt->execute(array($id,$user));
                       $check = $stmt -> rowCount();
           if($check == 1){
           $formError[]= "<div class='alert alert-danger'>user exist</div>";
           }
          
          if(empty($user)){
            $formError[] = '<div class="alert alert-danger">Username cant be <strong>empty</strong></div>';
          }
          if(empty($email)){
            $formError[] = '<div class="alert alert-danger">email cant be <strong>empty</strong></div>';
          }
          if(empty($name)){
            $formError[] = '<div class="alert alert-danger">fullname cant be <strong>empty</strong></div>';
          }/*
          if(!empty($avatarName) && !in_array($avatarExtension,$avatarAllowedExtension)){
            $formError[] = 'This Extension Not <strong>Allowed</strong>';
          }
          if($avatarSize > 524288){//102400
            $formError[] = 'The Size Can Not Be Larger Than<strong>512 KB</strong>';
          }
          
          foreach($formError as  $error){
            echo $error . "<br>";
          }
          redirectHome($theMsg,'back');
          if(empty($formError)){
            global $con;
            $stmt = $con ->prepare("UPDATE users SET Username = ?,email = ? , fullname = ? , Password = ?
                            WHERE userId = ? LIMIT 1");
                       $stmt->execute(array($user,$email,$name,$pass,$id));
                       echo "<div class='container'>";
                       $theMsg= "<div class='alert alert-success'>".$stmt->rowCount() . ' record updated</div>';
                       redirectHome($theMsg,'back');
                       echo "</div>";
          }   else {
            foreach($formError as  $error){
              $theMsg = $error . "<br>";
            }
            redirectHome($theMsg,'back');
          }       
          //update DB with these data=====================
          */
    echo "</div>";
 }//end Update
  //===============================================================================

  //=========Start delete Page====================================================
  function delete(){
    echo "<h1 class='text-center'>Delete Member</h1> <div class='container'>";
                    $userId= isset($_GET['userId']) && is_numeric($_GET['userId']) ? intval($_GET['userId']) : 0 ;
                   $check = checkItem("userId","users",$userId);
                   //if there is id show form   
                   if($check > 0){ 
                     global $con;
                    $stmt = $con ->prepare("DELETE FROM users WHERE userId = :zid ");
                    $stmt ->bindParam(':zid',$userId);
                    $stmt ->execute();
                    $theMsg= "<div class='alert alert-success'>".$stmt->rowCount() . ' record Deleted</div>';
                    redirectHome($theMsg,'back');
                   }
                   else { $theMsg= "<div class='alert alert-danger'>This is ID not Exist</div>";
                    redirectHome($theMsg);
                  } 
                   echo "</div>";
  }
  //===========End delete Page===========================================

   //===========Start Activate Page===========================================
function activate(){
  echo "<h1 class='text-center'>Activate Member</h1> <div class='container'>";
  $userId= isset($_GET['userId']) && is_numeric($_GET['userId']) ? intval($_GET['userId']) : 0 ;
  $check = checkItem("userId","users",$userId);
  if($check > 0){ 
  
   global $con;
   $stmt = $con ->prepare("UPDATE users SET regstate = 1 WHERE userId = ? LIMIT 1");
   $stmt->execute(array($userId));
   
  $theMsg= "<div class='alert alert-success'>".$stmt->rowCount() . ' User Activated</div>';
   redirectHome($theMsg,'back');
   
  }  else {
    $theMsg= "<div class='alert alert-danger'>This is ID not Exist</div>";
    redirectHome($theMsg);
  } 
  echo "</div>";
}
//===========End Activate Page===========================================
if (isset($_SESSION['username'])){
    include 'init.php';
   
    $do = isset($_GET['do']) ? $_GET['do'] : 'Manage';
    switch($do){
      case 'Manage': manage(); break;// end manage
      case 'Add': Add(); break;// end Add
      case 'insert':
                    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
                      insert();  
                    } else{
                      echo "<div class='container'>";
                      $theMsg = '<div class="alert alert-danger">Sorry You can\'t Browse This Page Directly</div>';
                      redirectHome($theMsg);
                      echo "</div>";
                    }
                    break;// end insert
      case 'Edit': 
                   $userId= isset($_GET['userId']) && is_numeric($_GET['userId']) ? intval($_GET['userId']) : 0 ;
                   Edit($userId);
                   break;// end Edit
      case 'Update': 
                    echo "<h1 class='text-center'>Update Member</h1>";
                    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
                      Update();
                      } else{
                        echo "<div class='container'>";
                        $theMsg = '<div class="alert alert-danger">Sorry You can\'t Browse This Page Directly</div>';
                              redirectHome($theMsg);
                              echo "</div>";
                      }
                   break;// end update
      case 'Delete': delete(); break;// end Delete
      //case 'view': view(); break;// end view
      case 'Activate': activate();break;// end Activate
      default : echo 'welcome to Default page';
  };

    include $tpl.'footer.php';
} else {
    header('location:index.php');
  exit();
}
ob_end_flush();
?>